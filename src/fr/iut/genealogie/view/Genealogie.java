package fr.iut.genealogie.view;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.AndroidException;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleCursorAdapter;
import fr.iut.genealogie.R;
import fr.iut.genealogie.db.PersonDbAdapter;
import fr.iut.genealogie.service.PopupService;
import fr.iut.genealogie.translation.Locale;
import fr.iut.genealogie.translation.Message;
import fr.iut.genealogie.translation.MessageKey;

public class Genealogie extends ListActivity {
	private Locale locale = Locale.EN;
	private Message message;
    private static final int DELETE_ID = Menu.FIRST;
    private static final int IMPORT_ID = Menu.FIRST + 1;
    private static final int IMPORT_ALL = Menu.FIRST + 2;
    private static final int DELETE_ALL = Menu.FIRST + 3;
    static final int PICK_CONTACT_REQUEST = 1;  // The request code
    
	/**
	 * Definition des cles pour les arguments pour la navigation
	 */
	public static final String NAME_EXTRA = "nameExtra";
    public static final String ID_EXTRA = "idExtra";

	private int mPersonNumber = 0;
	private PersonDbAdapter mDbHelper;
	
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        message = new Message(locale);
        setContentView(R.layout.genealogie_list);
        mDbHelper = new PersonDbAdapter(this);
        mDbHelper.open();
        Cursor c = mDbHelper.fetchAllPersons();
        mPersonNumber = c.getCount();
        fillData();
        registerForContextMenu(getListView());
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, R.string.menu_delete_person);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	boolean result = super.onCreateOptionsMenu(menu);
        menu.add(0,IMPORT_ID,0,R.string.menu_import);
        menu.add(0,IMPORT_ALL,0,R.string.menu_importAll);
        menu.add(0,DELETE_ALL,0,R.string.menu_deleteAll);
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
        case IMPORT_ID:
        	pickContact();
        	return true;
        case IMPORT_ALL:
        	pickAllContact();
        	return true;
	    case DELETE_ALL:
	    	deleteAllPerson();
	    	mPersonNumber = 0;
	    	return true;
		}
        return super.onOptionsItemSelected(item);
    

	}
	
    private void deleteAllPerson() {
    	mDbHelper.deleteAllPerson();
    	fillData();
    }

    /**
     * importContact
     */
    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(Phone.CONTENT_TYPE); // Show user only contacts with phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }
    
    private void pickAllContact() {
    	ContentResolver cr = getContentResolver();
    	Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
    	List<String[]> listPerson = new ArrayList<String[]>();
        if (cursor.getCount() > 0) {
		    while (cursor.moveToNext()) {
		        	int columnName = cursor.getColumnIndex(Phone.DISPLAY_NAME);
	                int columnPhone = cursor.getColumnIndex(Phone.NUMBER);
	                int columnAvatar = cursor.getColumnIndex(Phone.PHOTO_ID);
	                String nameToReturn = cursor.getString(columnName);
	                String phoneToReturn = cursor.getString(columnPhone);
	                String avatarToReturn = cursor.getString(columnAvatar);
	                listPerson.add(new String[]{nameToReturn, phoneToReturn, avatarToReturn});
	        }
        }
    	createPersonsImport(listPerson);

    }		
    
	private void createPersonsImport(List<String[]> listPerson) {
		int count = 0;
		for (String[] person : listPerson){
			try {
	        	mDbHelper.createPerson(person[0], person[1], person[2]);
	        	mPersonNumber += 1;
	        } catch ( AndroidException e){
	        	count +=1;
	        }
		}
        fillData();
        if ( mPersonNumber == 0 ){
        	PopupService.createPopUp(Genealogie.this, message.get(MessageKey.NO_CONTACT), message.get(MessageKey.ERROR)); 
        } else {
        	if ( count > 0 ){
        		if (count == mPersonNumber) {
        			PopupService.createPopUp(Genealogie.this, "Nothing to add.",  message.get(MessageKey.ERROR));
        		} else if ( count == 1 ){
        			PopupService.createPopUp(Genealogie.this, count + " of your contact already exist. It has not been add.",  message.get(MessageKey.ERROR));
	        	} else {
	        		PopupService.createPopUp(Genealogie.this, count + " of your contact already exist. They have not been add.",  message.get(MessageKey.ERROR));
	        	}
	        } 
        }
	}

	private void createPersonImport(String name, String phone, String avatar) {
        try {
        	mDbHelper.createPerson(name, phone, avatar);
        	mPersonNumber += 1;
        } catch ( AndroidException e){
        	PopupService.createPopUp(Genealogie.this, e.getMessage(),  message.get(MessageKey.ERROR));
        }
        fillData();
    }
	
    private void fillData() {
        Cursor c = mDbHelper.fetchAllPersons();
        startManagingCursor(c);
        String[] from = new String[] { PersonDbAdapter.KEY_NAME, PersonDbAdapter.KEY_PHONE};
        int[] to = new int[] { R.id.name, R.id.phone};
        SimpleCursorAdapter persons = new SimpleCursorAdapter(this, R.layout.genealogie_row, c, from, to);
        setListAdapter(persons);

        this.getListView().setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
				Cursor c = mDbHelper.fetchAllPersons();
				c.moveToPosition(position);
				int nameINT = c.getColumnIndex(PersonDbAdapter.KEY_NAME);
				int nameID= c.getInt(c.getColumnIndex(PersonDbAdapter.KEY_ROWID));
				String name =  c.getString(nameINT);
				Intent t = new Intent(Genealogie.this, InfoPerson.class);
				t.putExtra(NAME_EXTRA,name);
				t.putExtra(ID_EXTRA, nameID);
				startActivity(t);
			} 
        });     
    }
    
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
        case DELETE_ID:
            AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
            mDbHelper.deletePerson(info.id);
            mPersonNumber -= 1;
            fillData();
            return true;
        }
        return super.onContextItemSelected(item);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri contactUri = data.getData();
                String[] projection = {Phone.DISPLAY_NAME, Phone.NUMBER, Phone.PHOTO_ID};
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                int columnName = cursor.getColumnIndex(Phone.DISPLAY_NAME);
                int columnPhone = cursor.getColumnIndex(Phone.NUMBER);
                int columnAvatar = cursor.getColumnIndex(Phone.PHOTO_ID);
                String name = cursor.getString(columnName);
                String phone = cursor.getString(columnPhone);
                String avatar = cursor.getString(columnAvatar);
                createPersonImport(name, phone, avatar);
            }
        }
    }

}
