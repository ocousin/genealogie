package fr.iut.genealogie.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import fr.iut.genealogie.R;
import fr.iut.genealogie.db.PersonDbAdapter;

public class InfoPerson extends ListActivity {
	public static final String NAME_EXTRA = "nameExtra";
	public static final String ID_EXTRA = "idExtra";
	
	public static final int ADD_RELATION_ID = Menu.FIRST;
	public static final int DELETE_ALL_RELATION_ID = Menu.FIRST + 1;
	public static final int CHANGE_NAME = Menu.FIRST + 2;
	private PersonDbAdapter mDbHelper = null;
	public TextView name = null;
	private int idPerson = 0;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_person);
        mDbHelper = new PersonDbAdapter(this);
        mDbHelper.open();
        Intent intent = getIntent();
        name = (TextView) findViewById(R.id.name_person_id);
        if (intent != null) {
       	  name.setText(intent.getStringExtra(NAME_EXTRA));
       	  idPerson = intent.getIntExtra(ID_EXTRA, 0);
        }
        findRelation(idPerson);
        registerForContextMenu(getListView());
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		menu.add(0, ADD_RELATION_ID, 0, R.string.menu_add_relation);
		menu.add(0, DELETE_ALL_RELATION_ID, 0,
				R.string.menu_delete_all_relations);
		menu.add(0, CHANGE_NAME, 0, R.string.menu_changeName);
	      return result;
	    
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
        case ADD_RELATION_ID:
			Intent t = new Intent(InfoPerson.this, EditPerson.class);
			name = (TextView) findViewById(R.id.name_person_id);
			t.putExtra(NAME_EXTRA,name.getText());
			t.putExtra(ID_EXTRA, idPerson);
			startActivity(t);
      	return true;
        case DELETE_ALL_RELATION_ID:
        	mDbHelper.deleteAllRelation(idPerson);
        	findRelation(idPerson);
        return true;
        case CHANGE_NAME : 
        	Intent t2 = new Intent(InfoPerson.this, EditNamePerson.class);
			name = (TextView) findViewById(R.id.name_person_id);
			t2.putExtra(NAME_EXTRA,name.getText());
			t2.putExtra(ID_EXTRA, idPerson);
			startActivity(t2);
		return true;
    	}
        return super.onOptionsItemSelected(item);
    }
	
	public void findRelation(long id){
		LinkedHashMap<Integer, Integer> map = mDbHelper.getRelations(id);
		ArrayList<String> list = new ArrayList<String>();
		ArrayAdapter<String> array_adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				android.R.id.text1, list);
		String name = "";
		for (Map.Entry<Integer, Integer> hash : map.entrySet()) {
			name = mDbHelper.fetchPersonName(hash.getKey());
			switch (hash.getValue()) {
			case 0:
				list.add("Parent de "+name);
				break;
			case 1:
				list.add("Enfant de "+name);
				break;
			case 2:
				list.add("Conjoint de "+name);
				break;
			}
		}
		setListAdapter(array_adapter2);
		
		
		
		
	}

}
