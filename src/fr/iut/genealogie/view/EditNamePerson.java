package fr.iut.genealogie.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.AndroidException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import fr.iut.genealogie.R;
import fr.iut.genealogie.db.PersonDbAdapter;
import fr.iut.genealogie.service.PopupService;

public class EditNamePerson extends Activity {
	public static final String NAME_EXTRA = "nameExtra";
	public static final String ID_EXTRA = "idExtra";


	private PersonDbAdapter mDbHelper = null;
	private Button confirm = null;
	private EditText edittext = null;
	private int idPerson = 0;
	
	public TextView name = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_name_person);
        mDbHelper = new PersonDbAdapter(this);
        mDbHelper.open();
        
        Intent intent = getIntent();
        name = (TextView) findViewById(R.id.name_person_id);

        if (intent != null) {
     	  name.setText(intent.getStringExtra(NAME_EXTRA));
     	  idPerson = intent.getIntExtra(ID_EXTRA, 0);
     	  edittext = (EditText) findViewById(R.id.edittext_id);
        }
        confirm = (Button) findViewById(R.id.button_confirmation);
        confirm.setOnClickListener(new OnClickListener() {

        	@Override
        	public void onClick(View v) {
	    		try {
	    			String newName = edittext.getText().toString();
					mDbHelper.updatePerson(idPerson, newName);
	    				  				
						PopupService.createPopUp(EditNamePerson.this, "Nom changé", ":)");
						Intent t = new Intent(EditNamePerson.this, InfoPerson.class);
						t.putExtra(NAME_EXTRA, newName);
						t.putExtra(ID_EXTRA,  idPerson);
						startActivity(t);
					} catch (AndroidException e) {
						PopupService.createPopUp(EditNamePerson.this, "Echec de changement.", "Oops ...");
						e.printStackTrace();
					}
        	}

        });
    }
}
