package fr.iut.genealogie.view;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.AndroidException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import fr.iut.genealogie.R;
import fr.iut.genealogie.db.PersonDbAdapter;
import fr.iut.genealogie.service.PopupService;

public class EditPerson extends Activity {
	public static final String NAME_EXTRA = "nameExtra";
	public static final String ID_EXTRA = "idExtra";


	private PersonDbAdapter mDbHelper = null;
	private Button confirm = null;
	private int idPerson = 0;
	
	private Spinner spinnerPerson = null;
	private Spinner spinnerRelation = null;
	public TextView name = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_person);
        mDbHelper = new PersonDbAdapter(this);
        mDbHelper.open();
        
        Intent intent = getIntent();
        name = (TextView) findViewById(R.id.name_person_id);
        spinner();

        if (intent != null) {
     	  name.setText(intent.getStringExtra(NAME_EXTRA));
     	  idPerson = intent.getIntExtra(ID_EXTRA, 0);
        }
        confirm = (Button) findViewById(R.id.button_confirmation);
        confirm.setOnClickListener(new OnClickListener() {

        	@Override
        	public void onClick(View v) {
	    		long relationType = spinnerRelation.getSelectedItemId();
	    		long relationPerson = spinnerPerson.getSelectedItemId();
	    		int relationTypeInt = ((Long) relationType).intValue();
	    		int relationPersonInt = ((Long) relationPerson).intValue();
	    		if (idPerson != relationPerson) {
	    			try {
	    				mDbHelper.createRelation(idPerson, relationPersonInt,
								relationTypeInt);
	    				
	    				int realType = 0;
	    		    	switch(relationTypeInt){
	    		    	case 0 :
	    		    		realType = 1;
	    		    		break;
	    		    	case 1 : 
	    		    		realType = 0;
	    		    		break;
	    		    	case 2 :
	    		    		realType = 2;
	    		    		break;
	    		    	}
	    				
	    				mDbHelper.createRelation(relationPersonInt, idPerson, realType);
						PopupService.createPopUp(EditPerson.this, "Relation ajoutée", ":)");
						Intent t = new Intent(EditPerson.this, InfoPerson.class);
						t.putExtra(NAME_EXTRA, name.getText());
						t.putExtra(ID_EXTRA,  idPerson);
						startActivity(t);
					} catch (AndroidException e) {
						e.printStackTrace();
					}
	    		} else{
	    			PopupService.createPopUp(EditPerson.this, "Same person.", "Oops ...");
	        	}
        	}

        });
    }
    
    public void spinner(){
    	spinnerRelation = (Spinner) findViewById(R.id.spinner_relation_list);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_relation_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRelation.setAdapter(adapter);
        
		spinnerPerson = (Spinner) this.findViewById(R.id.spinner_persons_id);
        SimpleCursorAdapter sca = getDatas();
        sca.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPerson.setAdapter(sca);        
        
    }
	
	 
		private SimpleCursorAdapter getDatas() {
			Cursor c = mDbHelper.fetchAllPersons();
			startManagingCursor(c);
			String[] from = new String[] { PersonDbAdapter.KEY_NAME };
			int[] to = new int[] { android.R.id.text1 };

			SimpleCursorAdapter persons = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, c, from, to);

			return persons;
		}
		
}
