package fr.iut.genealogie.service;

import android.app.AlertDialog;
import android.content.Context;
import android.view.ContextThemeWrapper;
import fr.iut.genealogie.R;

public class PopupService {
	public static void createPopUp(Context c, String message, String title){
		 AlertDialog alertDialog1 =new AlertDialog.Builder(new ContextThemeWrapper(c, R.style.popup)).create();   // Assosier un titre         
		 alertDialog1.setTitle(title);            // Associer un message                    
		 alertDialog1.setMessage(message);           // Ajouter un icone         
		 alertDialog1.setIcon(R.drawable.icon);           // Affichage       
		 alertDialog1.show();
	}
}
