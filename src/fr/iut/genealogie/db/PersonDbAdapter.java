/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package fr.iut.genealogie.db;

import java.util.LinkedHashMap;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.AndroidException;
import android.util.Log;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations
 * for the notepad example, and gives the ability to list all notes as well as
 * retrieve or modify a specific note.
 * 
 * This has been improved from the first version of this tutorial through the
 * addition of better error handling and also using returning a Cursor instead
 * of using a collection of inner classes (which is less scalable and not
 * recommended).
 */
public class PersonDbAdapter {

    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_AVATAR = "avatar";
    public static final String KEY_ROWID = "_id";
    
	public static final String KEY_ID1 = "id1";
    public static final String KEY_ID2 = "id2";
    public static final String KEY_TYPE = "type";

    private static final String TAG = "PersonsDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_CREATE =
        "create table persons (" + KEY_ROWID +" integer primary key autoincrement, " + 
        						   KEY_NAME + " text not null, " + 
        						   KEY_PHONE + " text not null, "+
        						   KEY_AVATAR + " text);";
    
    private static final String DATABASE_CREATE2 =
            "create table relations (" + KEY_ROWID +" integer primary key autoincrement, " 
            							+ KEY_ID1 + " integer not null, "
            									+ ""+ KEY_ID2 + " integer not null, " 
            							+ KEY_TYPE + " integer not null);";


    private static final String DATABASE_NAME = "data";
    private static final String DATA_BASE_PERSON = "persons";
    private static final String DATABASE_TABLE_RELATIONS = "relations";
    private static final int DATABASE_VERSION = 8;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE2);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS persons");
            onCreate(db);
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     * 
     * @param ctx the Context within which to work
     */
    public PersonDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     * 
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public PersonDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }


    /**
     * Create a new person using the name provided. If the person is
     * successfully created return the new rowId for that person, otherwise return
     * a -1 to indicate failure.
     * 
     * @param name the name of the person
     * @return rowId or -1 if failed
     * @throws AndroidException 
     */
    public long createPerson(String name, String phone, String avatar) throws AndroidException {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_PHONE, phone);
        initialValues.put(KEY_AVATAR, avatar);
        Cursor cursor = fetchAllPersons();
        boolean exist = false;
        int indexPhone = cursor.getColumnIndex(KEY_PHONE);
        int indexName = cursor.getColumnIndex(KEY_NAME);
        if (cursor.getCount() > 0) {
		    while (cursor.moveToNext()) {
	            String valPhone = cursor.getString(indexPhone);
	            String valName = cursor.getString(indexName);
	            if ( valPhone.equals(phone) && valName.equals(name)){
	            	exist = true;
	            }
	        }
        }
        if (!exist){
        	return mDb.insert(DATA_BASE_PERSON, null, initialValues);
        } else {
        	throw new AndroidException("This person has already been imported");
        }
        
    }

    /**
     * Delete the persons with the given rowId
     * 
     * @param rowId id of note to delete
     * @return true if deleted, false otherwise
     */
    public boolean deletePerson(long rowId) {

        return mDb.delete(DATA_BASE_PERSON, KEY_ROWID + "=" + rowId, null) > 0;
    }
    
    /**
     * Delete all the persons 
     * 
     * @return true if deleted, false otherwise
     */
    public boolean deleteAllPerson() {

        return mDb.delete(DATA_BASE_PERSON, null , null) > 0;
    }
    
    /**
     * Return a Cursor over the list of all persons in the database
     * 
     * @return Cursor over all persons
     */
    public Cursor fetchAllPersons() {

        return mDb.query(DATA_BASE_PERSON, new String[] {KEY_ROWID, KEY_NAME, KEY_PHONE, KEY_AVATAR}, null, null, null, null, null);
    }

    /**
     * Return a Cursor positioned at the persons that matches the given rowId
     * 
     * @param rowId id of note to retrieve
     * @return Cursor positioned to matching note, if found
     * @throws SQLException if note could not be found/retrieved
     */
    public Cursor fetchPerson(long rowId) throws SQLException {

        Cursor mCursor =

            mDb.query(true, DATA_BASE_PERSON, new String[] {KEY_ROWID,
                    KEY_NAME, KEY_PHONE, KEY_AVATAR}, KEY_ROWID + "=" + rowId, null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
    

    /**
     * Update the note using the details provided. The persons to be updated is
     * specified using the rowId, and it is altered to use the name
     * values passed in
     * 
     * @param rowId id of persons to update
     * @param name value to set note body to
     * @return true if the persons was successfully updated, false otherwise
     */
    public boolean updatePerson(long rowId, String name) throws AndroidException{
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);

        return mDb.update(DATA_BASE_PERSON, args, KEY_ROWID + "=" + rowId, null) > 0;
    }
    
    /**
     * Create relation between two person
     * @param id1 first persons
     * @param id2 second person
     * @param type relation type
     * @throws AndroidException
     */
    public long createRelation(int id1, int id2, int type) throws AndroidException {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_ID1, id1);
        initialValues.put(KEY_ID2, id2);
        initialValues.put(KEY_TYPE, type);

    	return mDb.insert(DATABASE_TABLE_RELATIONS, null, initialValues);

        
    }    
    
    
    /**
     * Delete all the relation of a person 
     * 
     * @return true if deleted, false otherwise
     */
    public boolean deleteAllRelation(int idP) {

        return mDb.delete(DATABASE_TABLE_RELATIONS, KEY_ID1 + "=" + idP , null) > 0;
    }
    
    /**
     * Return all relation of a person
     * @param id
     * @return
     * @throws SQLException
     */
    @SuppressLint("UseSparseArrays")
	public LinkedHashMap<Integer, Integer> getRelations(long id) throws SQLException {

		LinkedHashMap<Integer, Integer> map = new LinkedHashMap<Integer, Integer>();
		Integer type, id2;
		Cursor c = mDb.query(DATABASE_TABLE_RELATIONS, new String[] {
				KEY_ID2, KEY_TYPE }, KEY_ID1 + "="
				+ id, null, null, null, null);

		if (c.moveToFirst()) {
			id2 = c.getColumnIndex(KEY_ID2);
			type = c.getColumnIndex(KEY_TYPE);
			
			do {
				Integer key = c.getInt(id2);
				Integer value = c.getInt(type);
				map.put(key, value);
				
			} while (c.moveToNext());
		}
		return map;
	}
    
    public String fetchPersonName(long rowId) throws SQLException {

		Cursor mCursor = fetchPerson(rowId);
		return mCursor.getString(mCursor.getColumnIndex(KEY_NAME));
	}
}
