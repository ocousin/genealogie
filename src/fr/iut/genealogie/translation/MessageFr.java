package fr.iut.genealogie.translation;

public class MessageFr {

	public static String translate(MessageKey mk) {
		switch ( mk ){
			case ERROR : return "Oops ...";
			case SUCCESS: return ":)";
			case NO_CONTACT: return "Aucuns contacts trouvés";
			case CONTACT_EXIST:return " de vos contacts existe déjà. Il n'a pas été ajouté à la liste.";
			case CONTACTS_EXIST: return " de vos contacts existe déjà. Ils n'ont pas été ajoutés à la liste.";
			default: return "NO TRANSLATION";
		}
		
	}

}
