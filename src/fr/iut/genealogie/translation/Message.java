package fr.iut.genealogie.translation;

public class Message {
	
	public Locale locale;
	
	public Message(Locale locale){
		this.locale = locale;
	}
	
	public String get(MessageKey mk){
		switch ( locale ){
		case FR : return MessageFr.translate(mk);
		case EN : return MessageEn.translate(mk);
		default : return MessageEn.translate(mk);
		}
	}

}
