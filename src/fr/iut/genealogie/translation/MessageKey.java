package fr.iut.genealogie.translation;

public enum MessageKey{
	/**
	 * Error title
	 */
	ERROR,
	/**
	 * succes title
	 */
	SUCCESS,
	/**
	 * no contact found
	 */
	NO_CONTACT,
	CONTACT_EXIST,
	CONTACTS_EXIST,
	;
}
