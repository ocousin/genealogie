package fr.iut.genealogie.translation;

public class MessageEn {
	
	public static String translate(MessageKey mk) {
		switch ( mk ){
			case ERROR : return "Oops ...";
			case SUCCESS: return ":)";
			case NO_CONTACT: return "No contacts has been found";
			case CONTACT_EXIST: return " of your contact already exist. It has not been add.";
			case CONTACTS_EXIST: return " of your contact already exist. They have not been add.";
			default: return "NO TRANSLATION";
		}
		
	}

}
